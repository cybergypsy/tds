// Fill out your copyright notice in the Description page of Project Settings.


#include "Interactable.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SceneComponent.h"
#include "Components/BoxComponent.h"

// Sets default values
AInteractable::AInteractable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>("RootComponent"));
	
	DefaultMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Floor"));
	DefaultMesh->SetupAttachment(RootComponent);
	TriggerZone = CreateDefaultSubobject<UBoxComponent>(TEXT("TriggerZone"));
	TriggerZone->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AInteractable::BeginPlay()
{
	Super::BeginPlay();
	TriggerZone->OnComponentBeginOverlap.AddDynamic(this, &AInteractable::HandleBeginOverlapTrigger);
	TriggerZone->OnComponentEndOverlap.AddDynamic(this, &AInteractable::HandleEndOverlapTrigger);
}

// Called every frame
void AInteractable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AInteractable::HandleBeginOverlapTrigger(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                                  UPrimitiveComponent* OtherComp,
                                                  int32 OtherBodyIndex,
                                                  bool bFromSweep,
                                                  const FHitResult& SweepResult)
{
	if (!IsValid(OtherActor))
		return;
	Interact(OtherActor);
}

void AInteractable::HandleEndOverlapTrigger(class UPrimitiveComponent* OverlappedComp,
												class AActor* OtherActor,
												class UPrimitiveComponent* OtherComp,
												int32 OtherBodyIndex)
{
	if (!IsValid(OtherActor))
		return;
	EndInteract(OtherActor);
}

void AInteractable::Interact(AActor* OtherActor)
{
}


void AInteractable::EndInteract(AActor* OtherActor)
{
}