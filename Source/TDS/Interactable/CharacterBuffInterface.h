// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "CharacterBuffInterface.generated.h"

class ABuffActor;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UCharacterBuffInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TDS_API ICharacterBuffInterface
{
	GENERATED_BODY()

public:
	ICharacterBuffInterface();
	
	virtual bool AddBuff(ABuffActor* Buff, float Rate = 2.f);
	virtual bool RemoveBuff(ABuffActor* Buff);

	virtual void ApplyAcceleration(const float Acceleration);

	virtual float GetMaxSpeed() const {	return MaxSpeed; }
	virtual float GetMinSpeed() const { return MinSpeed; }
	
	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
protected:
	TSet<ABuffActor*> Buffs;

	float MaxSpeed;
    float MinSpeed;
	float SpeedWithoutBorders;
};
