// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickUpClass.h"
#include "ItemSpawner.generated.h"

UCLASS()
class TDS_API AItemSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItemSpawner();

	UFUNCTION(BlueprintGetter, BlueprintCallable)
	TSubclassOf<APickUpClass> GetItemForSpawn() { return ItemForSpawn; }
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, BlueprintGetter=GetItemForSpawn)
	TSubclassOf<APickUpClass> ItemForSpawn;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* DefaultRoot;

	UFUNCTION()
	void SpawnItem();

	UFUNCTION()
	void HandleItemDestroyed(AActor* OtherActor = nullptr);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	FTimerHandle Timer;

};
