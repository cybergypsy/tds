// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Interactable.h"
#include "PickUpClass.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API APickUpClass : public AInteractable
{
	GENERATED_BODY()

public:
	APickUpClass();

    // Called every frame
    virtual void Tick(float DeltaTime) override;

protected:
	virtual void Interact(AActor* Actor) override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	FRotator RotationRate;

};
